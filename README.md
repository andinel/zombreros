# Zombreros

## How to compile/run 

1. Download and open the latest version of Android Studio: https://developer.android.com/studio#downloads
2. Clone the project 
3. Make sure you have an android phone or android emulator with an android version that have a higher API than 14.
4. Press the run button at the top of the Android Studio window to run the application
5. A menu asking you to choose to run the program on a connected android device or an emulator will pop up once you press run. Choose the device you want to use and press Ok.


## Code structure

- **model/:** Manages the application’s data, logic and rules
    - **component/:** Simple data structures without any methods or logic. 
    - **factories/:** Includes the entities inheriting from the components.
    - **system/:** The logic that modifies the data of the components
- **presenter/:** Manages the interaction between the model and the view, translating user actionsinto changes to the model or changes to the view
- **utils/:** Includes classes that manages sound, assets and Firebase communication including dataholders for highscores
- **view/:** Interacts with the user and displays some portion of the underlying data


