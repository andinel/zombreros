package com.zombreros.game.utils;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class for storing score data from the database
 */

public class HighScores {
public ArrayList<Score> highscores = new ArrayList<>();

public void addScore(String nickname, int score){
    highscores.add(new Score(nickname, score));
}

public Score getScore(int i){
    return highscores.get(i);
}

public int getSize(){
    return highscores.size();
}

public void sortArray(){
    Collections.sort(highscores);
}

public void printArray(){
    for(int i = 0; i < highscores.size(); i++){
        System.out.println("Nickname: " + highscores.get(i).nickname + ", score: " + highscores.get(i).score);
    }
}
}
