package com.zombreros.game.utils;

public class Score implements Comparable{

    public String nickname;
    public int score;

    public Score(String nickname, int score){
        this.nickname = nickname;
        this.score = score;
    }

    @Override
    public int compareTo(Object o) {
        int compareScore=((Score)o).score;
        /* Descending order*/
        return compareScore-this.score;
    }
}
