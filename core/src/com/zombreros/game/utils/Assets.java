package com.zombreros.game.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.zombreros.game.ZombrerosGame;

import java.util.Random;

public class Assets {
    public static TextureRegion bg;

    public static Texture sprites, desertTex, mexZombie, resumeIcon, pauseBackground, quit, instructions;

    public static TextureRegion wall;

    public static TextureRegion playerR;
    public static TextureRegion playerG;

    public static TextureRegion bullet;

    public static TextureRegion basicZombie1;
    public static TextureRegion basicZombie2;
    public static TextureRegion basicZombie3;
    public static TextureRegion basicZombie4;

    public static TextureRegion fastZombie1;
    public static TextureRegion fastZombie2;
    public static TextureRegion fastZombie3;
    public static TextureRegion fastZombie4;

    public static TextureRegion bigZombie;

    public static TextureRegion hpRed;
    public static TextureRegion hpGreen;

    public static Music menuMusic;
    public static Music gameMusic;
    public static Music shopMusic;
    public static Music gameOverMusic;

    // SOUND EFFECTS

    public static Sound zombieDeath1;
    public static Sound zombieDeath2;
    public static Sound zombieDeath3;
    public static Sound zombieDeath4;
    public static Sound zombieDeath5;

    public static Sound gunshot;
    public static Sound startWave;

    public static Sound buttonClick;
    public static Sound purchaseClick;

    private static Random random = new Random();


    public static void load() {
        bg = new TextureRegion(new Texture("dune.png"),0,800-ZombrerosGame.HEIGHT, ZombrerosGame.WIDTH + 200, ZombrerosGame.HEIGHT);

        sprites = new Texture("sprites.png");

        wall = new TextureRegion(sprites, 200, 0, 200, 200);

        playerR = new TextureRegion(sprites, 0, 0, 54, 43);
        playerG = new TextureRegion(sprites, 54, 0,54,43);

        bullet = new TextureRegion(sprites, 0, 50, 5, 5);

        basicZombie1 = new TextureRegion(sprites, 110, 0, 151-110, 43);
        basicZombie2 = new TextureRegion(sprites, 153, 0, 193-153, 43);
        basicZombie3 = new TextureRegion(sprites, 111, 46, 151-111, 84-46);
        basicZombie4 = new TextureRegion(sprites, 153, 46, 194-153, 84-46);

        fastZombie1 = new TextureRegion(sprites, 101, 107, 148 - 101, 153 - 107);
        fastZombie2 = new TextureRegion(sprites, 154, 107, 200 - 154, 153 - 107);
        fastZombie3 = new TextureRegion(sprites, 102, 164, 147 - 102, 212 - 164);
        fastZombie4 = new TextureRegion(sprites, 154, 167, 196 - 154, 225 - 167);

        bigZombie = new TextureRegion(sprites, 0, 60, 74, 124 - 60);

        hpRed = new TextureRegion(sprites, 110, 90, 41, 4);
        hpGreen = new TextureRegion(sprites, 110, 95, 41, 4);

        mexZombie = new Texture("mex-zombie-remove.png");
        desertTex = new Texture("desert-background.jpg");
        instructions = new Texture("instructions.jpg");

        resumeIcon = new Texture("resume-icon.png");
        pauseBackground = new Texture("background.png");
        quit = new Texture("quit.png");

        menuMusic = Gdx.audio.newMusic(Gdx.files.internal("Mexican_Hat_Dance_menu.mp3"));
        gameMusic = Gdx.audio.newMusic(Gdx.files.internal("Mexican_Hat_Dance_METAL.mp3"));
        shopMusic = Gdx.audio.newMusic(Gdx.files.internal("Mexican_Hat_Dance_partymix.mp3"));
        gameOverMusic = Gdx.audio.newMusic(Gdx.files.internal("Mexican_hat_dance.mp3"));

        zombieDeath1 = Gdx.audio.newSound(Gdx.files.internal("zombie_death1.wav"));
        zombieDeath2 = Gdx.audio.newSound(Gdx.files.internal("zombie_death2.wav"));
        zombieDeath3 = Gdx.audio.newSound(Gdx.files.internal("zombie_death3.wav"));
        zombieDeath4 = Gdx.audio.newSound(Gdx.files.internal("zombie_death4.mp3"));
        zombieDeath5 = Gdx.audio.newSound(Gdx.files.internal("zombie_death5.wav"));

        gunshot = Gdx.audio.newSound(Gdx.files.internal("gunshot.mp3"));
        startWave = Gdx.audio.newSound(Gdx.files.internal("start_wave.wav"));

        buttonClick = Gdx.audio.newSound(Gdx.files.internal("button-click.mp3"));
        purchaseClick = Gdx.audio.newSound(Gdx.files.internal("cash_register.mp3"));


    }

    public static TextureRegion getBasicZombie(){
        switch (random.nextInt(4)){
            case 0:
                return basicZombie1;
            case 1:
                return basicZombie2;
            case 2:
                return basicZombie3;
            case 3:
                return basicZombie4;
        }
        return basicZombie1;
    }

    public static TextureRegion getFastZombie(){
        switch (random.nextInt(4)){
            case 0:
                return fastZombie1;
            case 1:
                return fastZombie2;
            case 2:
                return fastZombie3;
            case 3:
                return fastZombie4;
        }
        return fastZombie1;
    }

    public static Sound getZombieDeathSound(){
        switch (random.nextInt(5)){
            case 0:
                return zombieDeath1;
            case 1:
                return zombieDeath2;
            case 2:
                return zombieDeath3;
            case 3:
                return zombieDeath4;
            case 5:
                return zombieDeath5;
        }
        return zombieDeath1;
    }

}
