package com.zombreros.game.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SoundInterfaceClass implements SoundInterface {

    private Music backgroundMusic;
    private Sound buttonSound;
    private boolean checked = true;
    private boolean checkedSound = true;



    @Override
    public void playMusic() {
        backgroundMusic = Assets.menuMusic;
        backgroundMusic.setVolume(0.05F);
        if (!backgroundMusic.isPlaying()) {
            backgroundMusic.play();
            backgroundMusic.setLooping(true);
        }
    }

    @Override
    public void changeToGameMusic() {
        if (backgroundMusic.isPlaying()) {
            backgroundMusic.pause();
        }
        if(getChecked()){
            backgroundMusic = Assets.gameMusic;
            backgroundMusic.setVolume(0.05F);
            backgroundMusic.play();
            backgroundMusic.setLooping(true);
        }
    }

    @Override
    public void changeToShopMusic() {
        if (backgroundMusic.isPlaying()) {
            backgroundMusic.pause();
        }
        if(getChecked()){
            backgroundMusic = Assets.shopMusic;
            backgroundMusic.setVolume(0.05F);
            backgroundMusic.play();
            backgroundMusic.setLooping(true);
        }
    }

    @Override
    public void changeToMenuMusic() {
        if (backgroundMusic.isPlaying()) {
            backgroundMusic.pause();
        }
        if(getChecked()){

            backgroundMusic = Assets.menuMusic;
            backgroundMusic.setVolume(0.05F);
            backgroundMusic.play();
            backgroundMusic.setLooping(true);
        }
    }

    @Override
    public void changeToGameOverMusic() {
        if (backgroundMusic.isPlaying()) {
            backgroundMusic.pause();
        }
        if(getChecked()){

            backgroundMusic = Assets.gameOverMusic;
            backgroundMusic.setVolume(0.10F);
            backgroundMusic.play();
            backgroundMusic.setLooping(true);
        }
    }

    @Override
    public void pauseMusic() {
        if (backgroundMusic.isPlaying()) {
            backgroundMusic.pause();
        }
    }

    @Override
    public boolean getChecked() {
        return checked;
    }

    @Override
    public void setChecked(boolean c) {
        checked = c;
        musicLogic();
    }

    public void musicLogic() {
        if (getChecked()) {
            playMusic();
        } else if (!getChecked()) {
            pauseMusic();
        }
    }

    //Sound
    @Override
    public void playButtonSound() {
        if(checkedSound){
            buttonSound = Assets.buttonClick;
            buttonSound.play(0.2F);
        }
    }

    @Override
    public void playPurchaseSound() {
        if(checkedSound){
            buttonSound = Assets.purchaseClick;
            buttonSound.play(0.2F);
        }
    }

    public void playZombieDeathSound() {
        if (checkedSound) {
            Sound zombieDeathSound = Assets.getZombieDeathSound();
            zombieDeathSound.play(0.15f);
        }
    }
    public void playGunshotSound() {
        if(checkedSound) {
            Sound zombieDeathSound = Assets.gunshot;
            zombieDeathSound.play(0.05f);
            }

    }

    @Override
    public void playStartWaveSound() {
        if (checkedSound) {
            pauseMusic();
            Assets.startWave.play(0.1f);
        }

    }


    @Override
    public boolean getCheckedSound() { return checkedSound; }

    @Override
    public void setCheckedSound(boolean c) { checkedSound = c; }

}

