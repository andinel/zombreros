package com.zombreros.game.utils;

public interface FireBaseInterface {

    void registerNewScore(String nickname, int score);

    void SetOnValueChangedListener(HighScores highscores);
}
