package com.zombreros.game.utils;


public interface SoundInterface {

    void playMusic();
    void changeToGameMusic();
    void changeToShopMusic();
    void changeToMenuMusic();
    void changeToGameOverMusic();
    void pauseMusic();
    boolean getChecked();
    void setChecked(boolean c);
    void musicLogic();
    void playZombieDeathSound();
    void playGunshotSound();
    void playStartWaveSound();

    void playButtonSound();
    void playPurchaseSound();
    boolean getCheckedSound();
    void setCheckedSound(boolean c);


}
