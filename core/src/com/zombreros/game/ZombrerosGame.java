package com.zombreros.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.zombreros.game.model.GameState;
import com.zombreros.game.presenter.GamePresenter;
import com.zombreros.game.presenter.MainMenuPresenter;
import com.zombreros.game.utils.Assets;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.SoundInterfaceClass;

/**
 * Main game class
 */
public class ZombrerosGame extends Game {

	public static final int WIDTH = 854;
	public static final int HEIGHT = 480;

	public static final String TITLE = "Zombreros";

	SpriteBatch batch;
	private Viewport viewport;

	private SoundInterfaceClass SIC;
	private FireBaseInterface FBIC;

	private TextureAtlas atlas;
	private Skin skin;

	public static Color BACKGROUND_COLOR = new Color(1, 1, 1, 1);
	public static Color SECONDARY_COLOR = new Color(0.5f, 0.8f, -0.5f, 1);

	private GamePresenter gamePresenter;


	public ZombrerosGame(FireBaseInterface FBIC, SoundInterfaceClass SIC) {
		this.FBIC = FBIC;
		this.SIC = SIC;
	}


	@Override
	public void create () {
		batch = new SpriteBatch();
		viewport = new ExtendViewport(WIDTH, HEIGHT);

		atlas = new TextureAtlas("style/skins/zombieskin1/zombie-skin1.atlas");
		skin = new Skin(Gdx.files.internal("style/skins/zombieskin1/zombie-skin1.json"), atlas);

		Assets.load();
		SIC.playMusic();

		setScreen(new MainMenuPresenter(this, FBIC, SIC));
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void dispose () {
		batch.dispose();
	}

	public Viewport getViewport() {
		return viewport;
	}

	public SpriteBatch getBatch() {
		return batch;
	}


	public TextureAtlas getAtlas() {
		return atlas;
	}

	public Skin getSkin() {
		return skin;
	}

	public void pause(){
		gamePresenter.setGameState(GameState.PAUSE);
	}


}
