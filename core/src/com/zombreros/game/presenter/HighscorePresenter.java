package com.zombreros.game.presenter;

import com.badlogic.gdx.ScreenAdapter;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.HighScores;
import com.zombreros.game.utils.SoundInterface;
import com.zombreros.game.view.HighscoreView;


public class HighscorePresenter extends ScreenAdapter {

    private ZombrerosGame zombreros;
    private FireBaseInterface FBIC;
    private SoundInterface SIC;
    private HighScores highscores;
    private HighscoreView view;


    public HighscorePresenter(ZombrerosGame zombreros, FireBaseInterface FBIC, SoundInterface SIC, HighScores highscores){
        this.zombreros = zombreros;
        this.FBIC = FBIC;
        this.SIC = SIC;
        this.highscores = highscores;
        this.view = new HighscoreView(zombreros, this);

    }

    @Override
    public void render(float delta){
        view.update(delta);
    }

    public HighScores getHighscores(){
        return highscores;
    }

    public void onMainMenu(){
        zombreros.setScreen(new MainMenuPresenter(zombreros, FBIC, SIC));
        SIC.playButtonSound();
    }



}
