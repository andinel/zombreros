package com.zombreros.game.presenter;

import com.badlogic.gdx.ScreenAdapter;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.SoundInterface;
import com.zombreros.game.view.InstructionsView;


public class InstructionsPresenter extends ScreenAdapter {

    private ZombrerosGame zombreros;
    private FireBaseInterface FBIC;
    private SoundInterface SIC;
    private InstructionsView view;

    public InstructionsPresenter(ZombrerosGame zombreros, FireBaseInterface FBIC, SoundInterface SIC){
        this.zombreros = zombreros;
        this.FBIC = FBIC;
        this.SIC = SIC;
        this.view = new InstructionsView(zombreros, this);

    }

    @Override
    public void render(float delta){
        view.update(delta);
    }

    public void onMainMenu(){
        zombreros.setScreen(new MainMenuPresenter(zombreros, FBIC, SIC));
        SIC.playButtonSound();
    }




}
