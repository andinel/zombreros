package com.zombreros.game.presenter;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.SoundInterface;
import com.zombreros.game.view.GameOverView;

public class GameOverPresenter extends ScreenAdapter {

    private ZombrerosGame zombreros;
    private FireBaseInterface FBIC;
    private SoundInterface SIC;
    private GameOverView view;
    private int score;

    public GameOverPresenter(ZombrerosGame zombreros, FireBaseInterface FBIC, SoundInterface SIC, int score){
        this.zombreros = zombreros;
        this.FBIC = FBIC;
        this.SIC = SIC;
        this.score = score;
        this.view = new GameOverView(zombreros, this, score);

    }

    @Override
    public void render(float delta){
        view.update(delta);
    }

    public void onSubmit(TextField nicknameField, int score){
        if(nicknameField.getText().trim().length() >= 3){
            FBIC.registerNewScore(nicknameField.getText(), score);
            zombreros.setScreen(new MainMenuPresenter(zombreros, FBIC, SIC));
            SIC.playButtonSound();
            SIC.changeToMenuMusic();

        }
        else{
            view.addErrorText();
        }
    }

    public void onSkip(){
        zombreros.setScreen(new MainMenuPresenter(zombreros, FBIC, SIC));
        SIC.playButtonSound();
        SIC.changeToMenuMusic();
    }

}
