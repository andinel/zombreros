package com.zombreros.game.presenter;

import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.utils.SoundInterface;
import com.zombreros.game.view.GameView;

public class LocalMultiplayerPresenter extends GamePresenter{


    public LocalMultiplayerPresenter(ZombrerosGame zombreros, GameView view, SoundInterface SIC){
        super(zombreros, view, SIC);
    }
}
