package com.zombreros.game.presenter;

import com.badlogic.gdx.ScreenAdapter;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.SoundInterface;
import com.zombreros.game.view.SettingsView;

public class SettingsPresenter extends ScreenAdapter {
    private boolean checked = true;
    private boolean checkedSound = true;

    private ZombrerosGame zombreros;
    private FireBaseInterface FBIC;
    private SoundInterface SIC;
    private SettingsView view;

    public SettingsPresenter(ZombrerosGame zombreros, FireBaseInterface FBIC, SoundInterface SIC){
        this.zombreros = zombreros;
        this.FBIC = FBIC;
        this.SIC = SIC;
        this.view = new SettingsView(zombreros, this);
    }

    @Override
    public void render(float delta){
        view.update(delta);
    }


    public void onMainMenu(){
        zombreros.setScreen(new MainMenuPresenter(zombreros, FBIC, SIC));
        SIC.playButtonSound();
    }

    //Music
    public void playMusic() {
        SIC.playMusic();
    }

    public void pauseMusic() {
        SIC.playMusic();
    }

    public boolean getChecked() {
        return SIC.getChecked();
    }

    public void setChecked(boolean c) {
        SIC.setChecked(c);
        checked = c;
        SIC.playButtonSound();
    }

    public void playSound() {
        SIC.playButtonSound();
    }

    public boolean getCheckedSound() { return SIC.getCheckedSound(); }

    public void setCheckedSound(boolean c) { SIC.setCheckedSound(c);
    checkedSound = c;
    playSound();}


}
