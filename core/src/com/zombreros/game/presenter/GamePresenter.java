package com.zombreros.game.presenter;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.model.GameState;
import com.zombreros.game.model.GameWorld;
import com.zombreros.game.utils.SoundInterface;
import com.zombreros.game.view.GameView;

//import com.zombreros.game.GameInputProcessor;

/**
 * Abstraction for a game presenter, a bridge between the model and view components
 */
public abstract class GamePresenter extends InputAdapter {

    /**
     * The main program class
     */
    protected ZombrerosGame zombreros;

    /**
     * The game model component
     */
    protected GameWorld gameModel;

    /**
     * The game view component
     */
    protected GameView view;

    /**
     * The game screen width
     */
    protected int width;


    SoundInterface SIC;


    public GamePresenter(ZombrerosGame zombreros, GameView view, SoundInterface SIC) {
        this.zombreros = zombreros;
        this.view = view;
        gameModel = new GameWorld(zombreros, SIC);
        refreshWidth();
    }

    public void refreshWidth() {
        width = zombreros.getViewport().getScreenWidth();
    }


    // ---- INPUT LOGIC ------
    // In reference to the observer pattern described in the Architectural document
    @Override
    public boolean touchDown (int x, int y, int pointer, int button) {
        if (!(getGameState() == GameState.PAUSE) && x < 3*width/5 && x > 2*width/5) {
            // pause
            setGameState(GameState.PAUSE);
        }
        else if (getGameState() == GameState.PAUSE && x < 4*width/6 && x > 2*width/6){
            // unpause
            setGameState(GameState.WAVE_STATE);
        }

        else if (getGameState() == GameState.PAUSE && x < view.quitButtonWidth && y > zombreros.getViewport().getScreenHeight()-view.quitButtonHeight ){
            // quit from pause
            setGameState(GameState.GAME_OVER);
        }
        return false;
    }

    // Handling swipe input, turning the players according to side of screen
    @Override
    public boolean touchDragged (int x, int y, int pointer) {
        if (x < width/2) {
            gameModel.getTurnsys().setTurn(Gdx.input.getDeltaY(pointer), 0);
        }
        else if (x > width/2) {
            gameModel.getTurnsys().setTurn(-Gdx.input.getDeltaY(pointer), 1);
        }
        return false;
    }
    // ----------------------------

    /** THIS IS KEPT IN CASE THERE ARE ISSUES WITH PRESENTER EXTENDING SCREENADAPTER - REMOVE LATER
     * Called if a swipe on the screen is detected
     * @param dy input's swiped distance on y axis
     * @param x input's coordinate on x axis

    public void handleSwipe(int x, int dy) {
        if (x > width/2) {
            gameModel.getTurnsys().setTurn(dy, 0);
        } else if (x < width/2) {
            gameModel.getTurnsys().setTurn(dy, 1);
        }
    }

    public void handleTouch(int x) {
        if (x < 3*width/5 && x > 2*width/5) {
            // Handle pause logic here
            Gdx.app.log("pause", "");
        }
    } */


    /**
     * Calls the game model update method when current player is ready
     */


    public void updateGame(float dt) {
        gameModel.updateGame(dt);
        view.setRenderQueue(gameModel.getRenderQueue());
        view.setScore(gameModel.getScore());
        view.setWaveNumber(gameModel.getWaveNumber());
    }
    public void pauseGame() {
        Gdx.app.log("pause", "");
    }

    /**
     * Game state methods so views keep track of the state of the game
     *
     */
    public GameState getGameState(){
        return gameModel.getGameState();
    }

    public void setGameState(GameState gameState) {
        gameModel.setGameState(gameState);

    }

    public GameView getView() {
        return view;
    }

    public  void setNextWave(){
        view.resetWaveDisplayTimer();
        gameModel.setNextWave();

    }
}
