package com.zombreros.game.presenter;

import com.badlogic.gdx.ScreenAdapter;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.HighScores;
import com.zombreros.game.utils.SoundInterface;
import com.zombreros.game.view.GameView;
import com.zombreros.game.view.MainMenuView;

public class MainMenuPresenter extends ScreenAdapter{

    private ZombrerosGame zombreros;
    private FireBaseInterface FBIC;
    private SoundInterface SIC;
    private HighScores highscores;
    private MainMenuView view;


    public MainMenuPresenter(ZombrerosGame zombreros, FireBaseInterface FBIC, SoundInterface SIC){
        this.zombreros = zombreros;
        this.FBIC = FBIC;
        this.SIC = SIC;
        this.view = new MainMenuView(zombreros, this);

        highscores = new HighScores();
        FBIC.SetOnValueChangedListener(highscores);
    }

    @Override
    public void render(float delta){
        view.update(delta);
    }


    public void startGame(){
        zombreros.setScreen(new GameView(zombreros, FBIC, SIC));
        SIC.pauseMusic();
        SIC.playButtonSound();
        SIC.playStartWaveSound();
    }

    public void onSettings(){
        zombreros.setScreen(new SettingsPresenter(zombreros, FBIC, SIC));
        SIC.playButtonSound();
    }

    public void onHighscores(){
        zombreros.setScreen(new HighscorePresenter(zombreros, FBIC, SIC, highscores));
        SIC.playButtonSound();
    }

    public void onInstructions(){
        zombreros.setScreen(new InstructionsPresenter(zombreros, FBIC, SIC));
        SIC.playButtonSound();
    }


}
