package com.zombreros.game.presenter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.model.Upgradeables;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.SoundInterface;
import com.zombreros.game.view.ShopView;

public class ShopPresenter extends ScreenAdapter {

    public Upgradeables upgradeables;
    private ZombrerosGame zombreros;
    private FireBaseInterface FBIC;
    private SoundInterface SIC;
    private ShopView view;
    GamePresenter gamePresenter;
    private int score;


    public ShopPresenter(ZombrerosGame zombreros, FireBaseInterface FBIC, SoundInterface SIC, GamePresenter gamePresenter, int score){
        this.upgradeables = upgradeables;
        this.zombreros = zombreros;
        this.FBIC = FBIC;
        this.SIC = SIC;
        this.gamePresenter = gamePresenter;
        this.view = new ShopView(zombreros, this, score);
        this.score = score;

    }

    @Override
    public void render(float delta) {
        view.update(delta);
    }

    public int getBulletUpgradePrice(){
        return upgradeables.BULLET_DAMAGE_LEVEL * upgradeables.PRICE_PER_LEVEL;
    }

    public int getFireRatePrice(){
        return upgradeables.BULLET_SPEED_LEVEL * upgradeables.PRICE_PER_LEVEL;
    }

    public void upgradeBulletDamage(){
        upgradeables.WALLET -= getBulletUpgradePrice();
        upgradeables.BULLET_DAMAGE += upgradeables.BULLET_DAMAGE_UPGRADE;
        upgradeables.BULLET_DAMAGE_LEVEL++;
    }

    public void upgradeFireRate(){
        upgradeables.WALLET -= getFireRatePrice();
        upgradeables.BULLET_SPEED += upgradeables.BULLET_SPEED_UPGRADE;
        upgradeables.BULLET_SPEED_LEVEL++;
    }

    public void upgradeWall(){
        upgradeables.WALLET -= getWallUpgradePrice();
        upgradeables.WALL_MAX_HP += upgradeables.WALL_UPGRADE;
        upgradeables.WALL_LEVEL++;
    }

    public boolean buyDamageUpgrade(){
        if((upgradeables.WALLET - getBulletUpgradePrice()) >= 0){
            upgradeBulletDamage();
            SIC.playPurchaseSound();
            return true;
        }
        else return false;
    }

    public boolean buyFireRateUpgrade(){
        if((upgradeables.WALLET - getFireRatePrice()) >= 0){
            upgradeFireRate();
            SIC.playPurchaseSound();
            return true;
        }
        else return false;
    }

    public boolean buyWallUpgrade(){
        if((upgradeables.WALLET - getWallUpgradePrice() >= 0)){
            upgradeWall();
            SIC.playPurchaseSound();
            return true;
        }
        else return false;
    }

    public boolean buyWallRepair(){
        if((upgradeables.WALLET - upgradeables.REPAIR_PRICE) >= 0){
            upgradeables.WALL_HP += (int)(upgradeables.WALL_MAX_HP * (upgradeables.REPAIR_PERCENTAGE/100.0));
            if(upgradeables.WALL_HP > upgradeables.WALL_MAX_HP){
                upgradeables.WALL_HP = upgradeables.WALL_MAX_HP;
            }
            upgradeables.WALLET -= upgradeables.REPAIR_PRICE;
            SIC.playPurchaseSound();
            return true;
        }
        else return false;
    }

    public int getWallUpgradePrice(){
        return upgradeables.WALL_LEVEL * upgradeables.PRICE_PER_LEVEL;
    }

    public void setNextWave(){
        gamePresenter.setNextWave();
        Gdx.input.setInputProcessor(gamePresenter);
        zombreros.setScreen(gamePresenter.getView());
        SIC.playButtonSound();
        SIC.playStartWaveSound();
    }

    public void onGameOver(){
        zombreros.setScreen(new GameOverPresenter(zombreros, FBIC, SIC, score));
        SIC.changeToGameOverMusic();
        SIC.playButtonSound();
    }


}
