package com.zombreros.game.view;


import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.model.GameState;
import com.zombreros.game.model.component.HPComponent;
import com.zombreros.game.model.component.PositionComponent;
import com.zombreros.game.model.component.RotationComponent;
import com.zombreros.game.model.component.TextureComponent;
import com.zombreros.game.presenter.GameOverPresenter;
import com.zombreros.game.presenter.GamePresenter;
import com.zombreros.game.presenter.LocalMultiplayerPresenter;
import com.zombreros.game.presenter.ShopPresenter;
import com.zombreros.game.utils.Assets;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.SoundInterface;


public class GameView extends ScreenAdapter {

    private final static int SCORE_FONT_SIZE = 80;
    private final static int WAVE_FONT_SIZE = 600;
    private final static float RGB_RED = 0.65f;
    private final static float RGB_GREEN = 0.0f;
    private final static float RGB_BLUE = 0.0f;
    private final static float WAVE_DISPLAY_TIMER = 5.0f;

    public static int quitButtonWidth = 150;
    public static int quitButtonHeight = 75;

    private GamePresenter presenter;

    private SpriteBatch sb;

    private BitmapFont font;

    private ImmutableArray<Entity> renderQueue;

    private float waveDisplayTimer;
    private int waveNumber;
    private int score;
    private int fontSize;

    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<TextureComponent> tm = ComponentMapper.getFor(TextureComponent.class);
    private ComponentMapper<RotationComponent> rm = ComponentMapper.getFor(RotationComponent.class);
    private ComponentMapper<HPComponent> hm = ComponentMapper.getFor(HPComponent.class);


    FireBaseInterface FBIC;
    SoundInterface SIC;
    ZombrerosGame zombreros;

    public GameView(ZombrerosGame zombreros, FireBaseInterface FBIC, SoundInterface SIC) {
        presenter = new LocalMultiplayerPresenter(zombreros, this, SIC);
        this.zombreros = zombreros;
        this.sb = zombreros.getBatch();
        this.FBIC = FBIC;
        this.SIC = SIC;

        Gdx.input.setInputProcessor(presenter);

        resetWaveDisplayTimer();

        fontSize = WAVE_FONT_SIZE;
        setFont(fontSize);

    }



    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(ZombrerosGame.BACKGROUND_COLOR.r, ZombrerosGame.BACKGROUND_COLOR.g, ZombrerosGame.BACKGROUND_COLOR.b, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );

        sb.begin();

        if (renderQueue.size()>0){
            for (Entity entity : renderQueue) {
                TextureComponent tex = tm.get(entity);
                PositionComponent pos = pm.get(entity);
                RotationComponent rot = rm.get(entity);



                TextureRegion reg = tex.getTexture();
                int width = reg.getRegionWidth();
                int height = reg.getRegionHeight();


                sb.draw(reg, pos.pos.x, pos.pos.y, (float) width/2, (float) height/2,
                        width, height, 1, 1, rot.angle);

                if (hm.has(entity)) {
                    HPComponent hel = hm.get(entity);
                    float hpMax = hel.hpMax;
                    float hp = hel.hp;
                    TextureRegion hpRed = hel.hpRed;
                    TextureRegion hpGreen = hel.hpGreen;

                    sb.draw(hpRed, pos.pos.x, pos.pos.y - 10,
                            reg.getRegionWidth(), hpRed.getRegionHeight());
                    sb.draw(hpGreen, pos.pos.x, pos.pos.y - 10,
                            reg.getRegionWidth()*(hp/hpMax), hpRed.getRegionHeight());

                }

                // scale x and y may be changed to a component so that each entity can be scaled

            }

            renderScreenHUD(delta);

        }



        if(presenter.getGameState() == GameState.PAUSE){
            sb.draw(Assets.pauseBackground, 0, 0);
            sb.draw(Assets.resumeIcon, (float) (ZombrerosGame.WIDTH/3.1), (float) (ZombrerosGame.HEIGHT/5));
            sb.draw(Assets.quit, 0, 0, quitButtonWidth, quitButtonHeight);

        }
        sb.end();

    }


    public void update(float dt) {
        switch (presenter.getGameState()) {
            case WAVE_STATE:
                presenter.updateGame(dt);
                break;
            case UPGRADE_STATE:
                SIC.changeToShopMusic();
                zombreros.setScreen(new ShopPresenter(zombreros, FBIC, SIC, presenter, score));
                break;
            case GAME_OVER:
                zombreros.setScreen(new GameOverPresenter(zombreros, FBIC, SIC, score));
                SIC.playButtonSound();
                SIC.changeToGameOverMusic();
                break;
        }
    }

    public void setRenderQueue(ImmutableArray<Entity> renderQueue) {
        this.renderQueue = renderQueue;
    }

    public void setScore(int score){
        this.score = score;
    }

    public void setWaveNumber(int number) {
        this.waveNumber = number;
    }

    private void setFont(int size) {
        // FreeType to convert TTF to BMFont
        Gdx.app.log("setFontSize", Integer.toString(size));
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("rm_typerighter_old.ttf"));
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = size;
        font = generator.generateFont(parameter);
        generator.dispose();
        font.setColor(RGB_RED, RGB_GREEN, RGB_BLUE,1);
    }

    public void resetWaveDisplayTimer() {
        waveDisplayTimer = WAVE_DISPLAY_TIMER;
    }

    private void renderScreenHUD(float dt) {
        if (waveDisplayTimer > 0) {


            waveDisplayTimer -= dt;

            if (fontSize != WAVE_FONT_SIZE) {
                fontSize = WAVE_FONT_SIZE;
                setFont(fontSize);
            }

            GlyphLayout layout = new GlyphLayout(font, Integer.toString(waveNumber));

            if (waveDisplayTimer < 1.0f) {
                // fade out
                font.setColor(RGB_RED, RGB_GREEN, RGB_BLUE, waveDisplayTimer);
            }

            font.draw(sb, layout, ZombrerosGame.WIDTH /2f - layout.width/2f, ZombrerosGame.HEIGHT/2f + layout.height/2f);

        } else {

            if (fontSize != SCORE_FONT_SIZE) {
                fontSize = SCORE_FONT_SIZE;
                setFont(fontSize);
                SIC.changeToGameMusic();
            }

            font.setColor(RGB_RED, RGB_GREEN, RGB_BLUE,1);
            GlyphLayout layout = new GlyphLayout(font, Integer.toString(score));

            font.draw(sb, layout, ZombrerosGame.WIDTH /2f - layout.width/2f, ZombrerosGame.HEIGHT - 30);
        }

    }

    @Override
    public void dispose(){
        font.dispose();
        sb.dispose();
    }

}
