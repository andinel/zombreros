package com.zombreros.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.presenter.ShopPresenter;
import com.zombreros.game.utils.Assets;

public class ShopView extends ScreenAdapter {
    private ZombrerosGame zombreros;
    private Stage stage;
    private Table buttonQuitTable, buttonContinueTable, titleTable, overviewTable, upgradeWallTable, upgradeWallTable2, upgradeGunTable, upgradeGunTable2, emptyWalletTable, backgroundTable, scoreTable;
    private Label mainTitle, overviewLabel, wallLabel, gunLabel, emptyWalletLabel, scoreLabel;
    private TextButton buttonContinue, buttonQuit, buttonUpgradeWallHP, buttonWallRepair, buttonUpgradeGunFirerate, buttonUpgradeGunDamage;
    private ShopPresenter presenter;
    private int score;

    public ShopView(ZombrerosGame zombreros, ShopPresenter presenter, int score) {
        this.zombreros = zombreros;
        this.presenter = presenter;
        this.score = score;
        stage = new Stage(zombreros.getViewport(), zombreros.getBatch());

        addBackground();
        addTitle();
        addOverview();
        addButtons();
        addUpgradeWallList();
        addUpgradeGunList();
        addListeners();

        Gdx.input.setInputProcessor(stage);
    }

    private void addBackground(){
        backgroundTable = new Table();
        backgroundTable.setFillParent(true);
        backgroundTable.add(new Image(Assets.desertTex));
        stage.addActor(backgroundTable);
    }

    public void update(float delta) {
        Gdx.gl.glClearColor(ZombrerosGame.BACKGROUND_COLOR.r,
                ZombrerosGame.BACKGROUND_COLOR.g,
                zombreros.BACKGROUND_COLOR.b,
                1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
        stage.draw();
    }

    private void addTitle() {
        mainTitle = new Label("Shop", zombreros.getSkin());
        mainTitle.setFontScale(2);

        titleTable = new Table();
        titleTable.setFillParent(true);
        titleTable.top();
        titleTable.add(mainTitle).padTop(50);

        stage.addActor(titleTable);

        emptyWalletTable = new Table();
        emptyWalletTable.setFillParent(true);
        emptyWalletTable.bottom().padBottom(130);
        emptyWalletLabel = new Label("Not enough coins for this upgrade", zombreros.getSkin(), "shop");
        emptyWalletLabel.setFontScale(1.5f);
        emptyWalletTable.add(emptyWalletLabel);
        stage.addActor(emptyWalletTable);
        emptyWalletTable.setVisible(false);

        scoreTable = new Table();
        scoreTable.setFillParent(true);
        scoreTable.top().left();

        scoreLabel = new Label("Score: " + score, zombreros.getSkin(), "typewriter");
        scoreTable.add(scoreLabel).padTop(30).padLeft(60).align(Align.right);

        stage.addActor(scoreTable);
    }

    private void addOverview(){
        overviewTable = new Table();
        overviewTable.setFillParent(true);
        overviewTable.top().right();

        overviewLabel = new Label("Coins: " + presenter.upgradeables.WALLET + "$", zombreros.getSkin(), "typewriter");
        overviewLabel.setFontScale(1);
        overviewTable.add(overviewLabel).padTop(30).padRight(60).align(Align.left);
        overviewTable.row();
        overviewLabel = new Label("Wall HP: " + presenter.upgradeables.WALL_HP+ "/" + presenter.upgradeables.WALL_MAX_HP, zombreros.getSkin(), "typewriter");
        overviewTable.add(overviewLabel).padTop(5).padRight(60).align(Align.left);

        stage.addActor(overviewTable);
    }

    private void addButtons() {
        buttonContinueTable = new Table();
        buttonContinueTable.bottom();
        buttonContinueTable.setFillParent(true);
        buttonContinueTable.padBottom(30).right();
        buttonContinue = new TextButton("\n Continue \n", zombreros.getSkin());
        buttonContinueTable.add(buttonContinue).padRight(30).width(150);
        stage.addActor(buttonContinueTable);

        buttonQuitTable = new Table();
        buttonQuitTable.bottom();
        buttonQuitTable.setFillParent(true);
        buttonQuitTable.padBottom(30).left();
        buttonQuit = new TextButton("\n Quit Game \n", zombreros.getSkin(), "quit");
        buttonQuitTable.add(buttonQuit).padLeft(30).width(150);
        stage.addActor(buttonQuitTable);


        upgradeWallTable2 = new Table();
        upgradeWallTable2.setFillParent(true);
        upgradeWallTable2.top().padTop(140);
        upgradeWallTable2.left().padLeft(210);
        buttonUpgradeWallHP = new TextButton( "+" + presenter.upgradeables.WALL_UPGRADE + " max HP\n \n " + presenter.getWallUpgradePrice() + "$", zombreros.getSkin());
        buttonUpgradeWallHP.getLabel().setFontScale(0.8f);
        upgradeWallTable2.add(buttonUpgradeWallHP).padTop(20).width(170).height(60);
        upgradeWallTable2.row();
        buttonWallRepair = new TextButton( "+" + presenter.upgradeables.REPAIR_PERCENTAGE + "% HP\n \n" + presenter.upgradeables.REPAIR_PRICE + "$", zombreros.getSkin());
        buttonWallRepair.getLabel().setFontScale(0.8f);
        upgradeWallTable2.add(buttonWallRepair).padTop(25).width(170).height(60);
        upgradeWallTable2.row();

        stage.addActor(upgradeWallTable2);

        upgradeGunTable2 = new Table();
        upgradeGunTable2.setFillParent(true);
        upgradeGunTable2.top().padTop(140);
        upgradeGunTable2.right().padRight(50);
        buttonUpgradeGunDamage = new TextButton("+" + presenter.upgradeables.BULLET_DAMAGE_UPGRADE + " damage\n \n" + presenter.getBulletUpgradePrice() +"$", zombreros.getSkin());
        buttonUpgradeGunDamage.getLabel().setFontScale(0.8f);
        upgradeGunTable2.add(buttonUpgradeGunDamage).padTop(20).width(170).height(60);
        upgradeGunTable2.row();
        buttonUpgradeGunFirerate = new TextButton("+" + presenter.upgradeables.BULLET_SPEED_UPGRADE + " speed\n \n" + presenter.getFireRatePrice() +"$", zombreros.getSkin());
        buttonUpgradeGunFirerate.getLabel().setFontScale(0.8f);
        upgradeGunTable2.add(buttonUpgradeGunFirerate).padTop(25).width(170).height(60);
        upgradeGunTable2.row();

        stage.addActor(upgradeGunTable2);
    }

    private void addUpgradeWallList(){

        upgradeWallTable = new Table();
        upgradeWallTable.setFillParent(true);
        upgradeWallTable.top().padTop(120);
        upgradeWallTable.left().padLeft(65);
        wallLabel = new Label("Wall lvl " + presenter.upgradeables.WALL_LEVEL + ": \n" + presenter.upgradeables.WALL_MAX_HP +" max HP", zombreros.getSkin(), "typewriter");
        wallLabel.setFontScale(1f);
        upgradeWallTable.add(wallLabel).padTop(50).left().align(Align.left);
        upgradeWallTable.row();
        wallLabel = new Label("Repair kit:", zombreros.getSkin(), "typewriter");
        wallLabel.setFontScale(1f);
        upgradeWallTable.add(wallLabel).padTop(60).left().align(Align.left);
        upgradeWallTable.row();

        stage.addActor(upgradeWallTable);

    }

    private void addUpgradeGunList(){

        upgradeGunTable = new Table();
        upgradeGunTable.setFillParent(true);
        upgradeGunTable.top().padTop(120);
        upgradeGunTable.right().padRight(230);
        gunLabel = new Label("Gun power lvl " + presenter.upgradeables.BULLET_DAMAGE_LEVEL + ": \n" + presenter.upgradeables.BULLET_DAMAGE + " damage", zombreros.getSkin(), "typewriter");
        gunLabel.setFontScale(1f);
        upgradeGunTable.add(gunLabel).padTop(50).left().align(Align.left);
        upgradeGunTable.row();
        gunLabel = new Label("Fire rate lvl " + presenter.upgradeables.BULLET_SPEED_LEVEL + ": \n" + presenter.upgradeables.BULLET_SPEED + " speed", zombreros.getSkin(), "typewriter");
        gunLabel.setFontScale(1f);
        upgradeGunTable.add(gunLabel).padTop(50).left().align(Align.left);
        upgradeGunTable.row();

        stage.addActor(upgradeGunTable);

    }

    public void updateButtonLabels(){
        buttonUpgradeGunDamage.setText("+" + presenter.upgradeables.BULLET_DAMAGE_UPGRADE + " damage\n \n" + presenter.getBulletUpgradePrice() +"$");
        buttonUpgradeGunFirerate.setText("+" + presenter.upgradeables.BULLET_SPEED_UPGRADE + " speed\n \n" + presenter.getFireRatePrice() +"$");
        buttonUpgradeWallHP.setText("+" + presenter.upgradeables.WALL_UPGRADE + " max HP\n \n " + presenter.getWallUpgradePrice() + "$");
        buttonWallRepair.setText("+" + presenter.upgradeables.REPAIR_PERCENTAGE + "% HP\n \n" + presenter.upgradeables.REPAIR_PRICE + "$");
    }

    public void updateOverview(){
        overviewTable = new Table();
        overviewTable.setFillParent(true);
        overviewTable.top().right();

        overviewLabel = new Label("Coins: " + presenter.upgradeables.WALLET + "$", zombreros.getSkin(), "shop");
        overviewLabel.setFontScale(1);
        overviewTable.add(overviewLabel).padTop(30).padRight(60).align(Align.left);
        overviewTable.row();
        overviewLabel = new Label("Wall HP: " +  presenter.upgradeables.WALL_HP + "/" + presenter.upgradeables.WALL_MAX_HP, zombreros.getSkin(), "typewriter");
        overviewTable.add(overviewLabel).padTop(5).padRight(60).align(Align.left);

        emptyWalletTable.setVisible(true);

        stage.addActor(overviewTable);
        stage.addActor(emptyWalletTable);

    }


    private void addListeners() {
        buttonContinue.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.setNextWave();
                return true;
            }
        });
        buttonQuit.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onGameOver();
                return true;
            }
        });
        buttonUpgradeWallHP.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (presenter.buyWallUpgrade()){
                    upgradeWallTable.setVisible(false);
                    addUpgradeWallList();
                    overviewTable.setVisible(false);
                    addOverview();
                    updateButtonLabels();
                    emptyWalletTable.setVisible(false);
                }
                else{
                    overviewTable.setVisible(false);
                    updateOverview();
                    buttonUpgradeWallHP.setStyle(zombreros.getSkin().get("shop", TextButton.TextButtonStyle.class));
                }
                return true;
            }
        });
        buttonWallRepair.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(presenter.buyWallRepair()){
                    overviewTable.setVisible(false);
                    addOverview();
                    updateButtonLabels();
                    emptyWalletTable.setVisible(false);
                }
                else{
                    overviewTable.setVisible(false);
                    updateOverview();;
                    buttonWallRepair.setStyle(zombreros.getSkin().get("shop", TextButton.TextButtonStyle.class));
                }
                return true;
            }
        });
        buttonUpgradeGunDamage.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(presenter.buyDamageUpgrade()){
                    upgradeGunTable.setVisible(false);
                    addUpgradeGunList();
                    overviewTable.setVisible(false);
                    addOverview();
                    updateButtonLabels();
                    emptyWalletTable.setVisible(false);
                }
                else{
                    overviewTable.setVisible(false);
                    updateOverview();
                    buttonUpgradeGunDamage.setStyle(zombreros.getSkin().get("shop", TextButton.TextButtonStyle.class));
                }
                return true;
            }
        });
        buttonUpgradeGunFirerate.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(presenter.buyFireRateUpgrade()){
                    upgradeGunTable.setVisible(false);
                    addUpgradeGunList();
                    overviewTable.setVisible(false);
                    addOverview();
                    updateButtonLabels();
                    emptyWalletTable.setVisible(false);
                }
                else{
                    overviewTable.setVisible(false);
                    updateOverview();
                    buttonUpgradeGunFirerate.setStyle(zombreros.getSkin().get("shop", TextButton.TextButtonStyle.class));
                }
                return true;
            }
        });

    }


}
