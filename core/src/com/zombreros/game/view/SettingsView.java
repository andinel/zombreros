package com.zombreros.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.presenter.SettingsPresenter;
import com.zombreros.game.utils.Assets;

public class SettingsView {
    private ZombrerosGame zombreros;
    private Stage stage;
    private Table buttonTable, backgroundTable;
    private Table titleTable;
    private Table contentTable;
    private Label mainTitle;
    private TextButton buttonBack;
    public CheckBox soundCheckBox;
    public CheckBox musicCheckBox;
    private SettingsPresenter presenter;


    public SettingsView(ZombrerosGame zombreros, SettingsPresenter presenter) {
        this.zombreros = zombreros;
        this.presenter = presenter;
        stage = new Stage(zombreros.getViewport(), zombreros.getBatch());

        addBackground();
        addTitle();
        addCheckbox();
        addButtons();
        addListeners();
        Gdx.input.setInputProcessor(stage);
    }

    private void addBackground(){
        backgroundTable = new Table();
        backgroundTable.setFillParent(true);
        backgroundTable.add(new Image(Assets.desertTex));
        stage.addActor(backgroundTable);
    }


    public void update(float delta) {
        Gdx.gl.glClearColor(ZombrerosGame.BACKGROUND_COLOR.r,
                ZombrerosGame.BACKGROUND_COLOR.g,
                zombreros.BACKGROUND_COLOR.b,
                1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
        stage.draw();
    }

    private void addTitle() {
        mainTitle = new Label("Settings", zombreros.getSkin());
        mainTitle.setFontScale(2);

        titleTable = new Table();
        titleTable.setFillParent(true);
        titleTable.top();
        titleTable.add(mainTitle).padTop(50);

        stage.addActor(titleTable);
    }

    private void addCheckbox() {
        contentTable = new Table();
        contentTable.setFillParent(true);
        contentTable.padBottom(60);

        soundCheckBox = new CheckBox("  Sound", zombreros.getSkin());
        contentTable.add(soundCheckBox).padBottom(20);
        contentTable.row();

        musicCheckBox = new CheckBox("  Music", zombreros.getSkin());
        contentTable.add(musicCheckBox).padBottom(20);
        contentTable.row();

        stage.addActor(contentTable);
    }

    private void addButtons() {
        buttonTable = new Table();
        buttonTable.bottom();
        buttonTable.setFillParent(true);
        buttonTable.padBottom(30).right();

        buttonBack = new TextButton("\n Back \n", zombreros.getSkin(), "default");
        buttonTable.add(buttonBack).padRight(30).width(150);
        buttonTable.row();

        stage.addActor(buttonTable);
    }


    public void addListeners() {

        buttonBack.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onMainMenu();
                return true;
            }
        });
        musicCheckBox.setChecked(presenter.getChecked());
        musicCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                presenter.setChecked(musicCheckBox.isChecked());
            }
        });
        soundCheckBox.setChecked(presenter.getCheckedSound());
        soundCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                presenter.setCheckedSound(soundCheckBox.isChecked());
            }
        });

    }






}


