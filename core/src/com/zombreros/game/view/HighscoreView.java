package com.zombreros.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.presenter.HighscorePresenter;
import com.zombreros.game.utils.Assets;
import com.zombreros.game.utils.HighScores;

public class HighscoreView {
    private ZombrerosGame zombreros;
    private Stage stage;
    private Table buttonTable, titleTable, rankHeader, nicknameHeader, scoreHeader, backgroundTable;
    private Label mainTitle, rankHeaderlabel, nicknameHeaderlabel, scoreHeaderlabel, rankLabel, nicknameLabel, scoreLabel;
    private TextButton buttonBack;

    private HighScores highscores;
    private HighscorePresenter presenter;

    public HighscoreView(ZombrerosGame zombreros, HighscorePresenter presenter) {
        this.zombreros = zombreros;
        this.presenter = presenter;

        stage = new Stage(zombreros.getViewport(), zombreros.getBatch());

        addBackground();
        highscores = presenter.getHighscores();
        addTitle();
        addButtons();
        addHighscoreList();
        addListeners();

        Gdx.input.setInputProcessor(stage);
    }

    private void addBackground(){
        backgroundTable = new Table();
        backgroundTable.setFillParent(true);
        backgroundTable.add(new Image(Assets.desertTex));
        stage.addActor(backgroundTable);
    }


    public void update(float delta) {
        Gdx.gl.glClearColor(ZombrerosGame.BACKGROUND_COLOR.r,
                ZombrerosGame.BACKGROUND_COLOR.g,
                zombreros.BACKGROUND_COLOR.b,
                1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
        stage.draw();
    }

    private void addTitle() {
        mainTitle = new Label("Highscore", zombreros.getSkin());
        mainTitle.setFontScale(2);

        titleTable = new Table();
        titleTable.setFillParent(true);
        titleTable.top();
        titleTable.add(mainTitle).padTop(50);

        stage.addActor(titleTable);
    }

    private void addButtons() {
        buttonTable = new Table();
        buttonTable.bottom();
        buttonTable.setFillParent(true);
        buttonTable.padBottom(30).right();

        buttonBack = new TextButton("\n Back \n", zombreros.getSkin());
        buttonTable.add(buttonBack).padRight(30).width(150);
        buttonTable.row();

        stage.addActor(buttonTable);
    }

    private void addHighscoreList(){
        //Creating table for rank
        rankHeader = new Table();
        rankHeader.setFillParent(true);
        rankHeader.top().padTop(70);
        rankHeader.left().padLeft(150);
        rankHeaderlabel = new Label("Rank", zombreros.getSkin());
        rankHeaderlabel.setFontScale(1.0f);
        rankHeader.add(rankHeaderlabel).padTop(30);
        rankHeader.row();

        //Creating table for nicknames
        nicknameHeader = new Table();
        nicknameHeader.setFillParent(true);
        nicknameHeader.top().padTop(70);
        nicknameHeader.center().top();
        nicknameHeaderlabel = new Label("Nickname", zombreros.getSkin());
        nicknameHeaderlabel.setFontScale(1.0f);
        nicknameHeader.add(nicknameHeaderlabel).padTop(30);
        nicknameHeader.row();

        //Creating table for scores
        scoreHeader = new Table();
        scoreHeader.setFillParent(true);
        scoreHeader.top().padTop(70);
        scoreHeader.right().padRight(150);
        scoreHeaderlabel = new Label("Score", zombreros.getSkin());
        scoreHeaderlabel.setFontScale(1.0f);
        scoreHeader.add(scoreHeaderlabel).padTop(30);
        scoreHeader.row();

        //Displaying up to 10 of the top registered highscores
        for(int i =0; i < highscores.getSize(); i++){
            rankLabel = new Label(Integer.toString(i+1), zombreros.getSkin(), "typewriter");
            rankLabel.setFontScale(0.75f);
            rankHeader.add(rankLabel).padTop(15);
            rankHeader.row();

            nicknameLabel = new Label(highscores.getScore(i).nickname, zombreros.getSkin(), "typewriter");
            nicknameLabel.setFontScale(0.75f);
            nicknameHeader.add(nicknameLabel).padTop(15);
            nicknameHeader.row();

            scoreLabel = new Label(Integer.toString(highscores.getScore(i).score), zombreros.getSkin(), "typewriter");
            scoreLabel.setFontScale(0.75f);
            scoreHeader.add(scoreLabel).padTop(15);
            scoreHeader.row();
        }

        //if data is not collected from firebase, write an error message
        if(highscores.highscores.isEmpty()){
            Label errorLabel = new Label("Oops. Something went wrong", zombreros.getSkin());
            errorLabel.setFontScale(1);
            nicknameHeader.add(errorLabel).padTop(100);
            nicknameHeader.row();

            Label errorLabel2 = new Label("Try to reload the page", zombreros.getSkin());
            errorLabel2.setFontScale(1);
            nicknameHeader.add(errorLabel2).padTop(50);
        }

        stage.addActor(scoreHeader);
        stage.addActor(nicknameHeader);
        stage.addActor(rankHeader);
    }

    private void addListeners() {
        buttonBack.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onMainMenu();
                return true;
            }
        });

    }


}
