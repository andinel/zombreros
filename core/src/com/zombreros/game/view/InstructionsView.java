package com.zombreros.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.presenter.InstructionsPresenter;
import com.zombreros.game.utils.Assets;


public class InstructionsView extends ScreenAdapter {
    private ZombrerosGame zombreros;
    private Stage stage;
    private Table buttonTable, titleTable, textTable, backgroundTable;
    private Label mainTitle, textContent;
    private TextButton buttonBack;
    private InstructionsPresenter presenter;

    public InstructionsView(ZombrerosGame zombreros, InstructionsPresenter presenter) {
        this.zombreros = zombreros;
        this.presenter = presenter;

        stage = new Stage(zombreros.getViewport(), zombreros.getBatch());
        addBackground();
        addTitle();
        addText();
        addButtons();
        addListeners();

        Gdx.input.setInputProcessor(stage);
    }

    private void addBackground(){
        backgroundTable = new Table();
        backgroundTable.setFillParent(true);
        backgroundTable.add(new Image(Assets.desertTex));
        stage.addActor(backgroundTable);
    }

    public void update(float delta) {
        Gdx.gl.glClearColor(ZombrerosGame.BACKGROUND_COLOR.r,
                ZombrerosGame.BACKGROUND_COLOR.g,
                zombreros.BACKGROUND_COLOR.b,
                1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
        stage.draw();
    }

    private void addTitle() {
        mainTitle = new Label("Instructions", zombreros.getSkin());
        mainTitle.setFontScale(2);

        titleTable = new Table();
        titleTable.setFillParent(true);
        titleTable.top();
        titleTable.add(mainTitle).padTop(50);

        stage.addActor(titleTable);
    }

    private void addText() {
        textTable = new Table();
        textTable.setFillParent(true);
        textTable.left().padBottom(60).padTop(50);

        String text = " This is a two player game." +
                "\n\n Place the phone so that one player use the top of the phone, and the other use the bottom. " +
                "\n\n Press start. The number in the middle of the screen represents the wave number. " +
                "\n\n After the number disappears, the game starts. The goal is to kill all zombies to complete a wave. " +
                "\n\n Control the bullets by swiping left/right as illustrated." +
                "\n\n You can upgrade your weapons and wall between waves." +
                "\n\n The game becomes more difficult for each wave. " +
                "\n\n The game is over when your wall reaches 0 HP.";

        Image controls = new Image(Assets.instructions);
        controls.setWidth(zombreros.WIDTH/2.2f);
        controls.setHeight(zombreros.HEIGHT/2.2f);
        controls.setPosition(zombreros.WIDTH/2f,zombreros.HEIGHT/3.9f, Align.center);

        stage.addActor(controls);

        textContent = new Label(text, zombreros.getSkin(), "typewriter");
        textContent.setFontScale(0.7f);
        textTable.add(textContent).padBottom(150).padLeft(50);
        textTable.row();


        stage.addActor(textTable);
    }

    private void addButtons() {
        buttonTable = new Table();
        buttonTable.bottom();
        buttonTable.setFillParent(true);
        buttonTable.padBottom(30).right();


        buttonBack = new TextButton("\n Back \n", zombreros.getSkin(), "default");
        buttonTable.add(buttonBack).padRight(30).width(150);
        buttonTable.row();

        stage.addActor(buttonTable);
    }


    private void addListeners() {
        buttonBack.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onMainMenu();
                return true;
            }
        });


    }



}
