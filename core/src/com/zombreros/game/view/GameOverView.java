package com.zombreros.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.presenter.GameOverPresenter;
import com.zombreros.game.utils.Assets;

public class GameOverView extends ScreenAdapter {
    private ZombrerosGame zombreros;
    private Stage stage;
    private Table buttonTable, titleTable, submitTable, errorTable, backgroundTable;
    private Label mainTitle, scoreLabel, nicknameLabel, errorLabel;
    private TextButton buttonBack, buttonSkip;
    private TextField nicknameField;
    private int endScore;
    private GameOverPresenter presenter;

    public GameOverView(ZombrerosGame zombreros, GameOverPresenter presenter, int endScore) {
        this.zombreros = zombreros;
        this.presenter = presenter;
        this.endScore = endScore;
        stage = new Stage(zombreros.getViewport(), zombreros.getBatch());

        addBackground();
        addTitle();
        addTextField();
        addButtons();
        addListeners();

        Gdx.input.setInputProcessor(stage);
    }

    private void addBackground(){
        backgroundTable = new Table();
        backgroundTable.setFillParent(true);
        backgroundTable.add(new Image(Assets.desertTex));
        stage.addActor(backgroundTable);
    }

    public void update(float delta) {
        Gdx.gl.glClearColor(ZombrerosGame.BACKGROUND_COLOR.r,
                ZombrerosGame.BACKGROUND_COLOR.g,
                zombreros.BACKGROUND_COLOR.b,
                1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
        stage.draw();
        stage.act();
    }

    private void addTitle() {
        mainTitle = new Label("Game Over", zombreros.getSkin());
        mainTitle.setFontScale(2.5f);

        titleTable = new Table();
        titleTable.setFillParent(true);
        titleTable.top();
        titleTable.add(mainTitle).expandX().padTop(100);

        stage.addActor(titleTable);
    }

    private void addTextField(){
        nicknameField = new TextField("", zombreros.getSkin());
        nicknameField.setMaxLength(12);
        nicknameField.setWidth(500);

        scoreLabel = new Label("Your score is " + endScore, zombreros.getSkin(), "typewriter");
        nicknameLabel = new Label("Enter your name ", zombreros.getSkin(), "typewriter");

        submitTable = new Table();
        submitTable.setFillParent(true);
        submitTable.top().padTop(200);

        submitTable.add(scoreLabel).pad(20);
        submitTable.row();
        submitTable.add(nicknameLabel);
        submitTable.add(nicknameField);

        stage.addActor(submitTable);

    }

    private void addButtons() {
        buttonTable = new Table();
        buttonTable.bottom();
        buttonTable.setFillParent(true);
        buttonTable.padBottom(50);

        buttonBack = new TextButton("\n Submit \n", zombreros.getSkin());
        buttonSkip = new TextButton("\n Skip \n", zombreros.getSkin());

        buttonTable.add(buttonSkip).center().width(200).pad(35);

        buttonTable.add(buttonBack).center().width(200).pad(35);


        buttonTable.row();
        stage.addActor(buttonTable);
    }

    public void addErrorText(){
        errorTable = new Table();
        errorTable.setFillParent(true);
        errorTable.bottom().padBottom(150);
        errorLabel = new Label("Your name needs to be at least 3 characters long", zombreros.getSkin(), "shop");
        errorTable.add(errorLabel);

        stage.addActor(errorTable);

    }

    private void addListeners() {
        buttonBack.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onSubmit(nicknameField, endScore);
                return true;
            }
        });

        buttonSkip.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onSkip();
                return true;
            }
        });

    }



}
