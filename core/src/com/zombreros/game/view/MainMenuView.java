package com.zombreros.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.presenter.MainMenuPresenter;
import com.zombreros.game.utils.Assets;


public class MainMenuView {

    private ZombrerosGame zombreros;
    private Stage stage;
    private Table buttonTable, titleTable;
    public Table imageTable, backgroundTable;
    private TextButton buttonPlay, buttonHighscore, buttonSettings, buttonInstructions;
    private Label mainTitle;
    private MainMenuPresenter presenter;

    public MainMenuView(ZombrerosGame zombreros, MainMenuPresenter presenter) {
        this.zombreros = zombreros;
        this.presenter = presenter;
        stage = new Stage(zombreros.getViewport(), zombreros.getBatch());
        addBackground();
        addImage();
        addTitle();
        addButtons();
        addListeners();

        Gdx.input.setInputProcessor(stage);
    }


    private void addBackground(){
        backgroundTable = new Table();
        backgroundTable.setFillParent(true);
        backgroundTable.add(new Image(Assets.desertTex));
        stage.addActor(backgroundTable);
    }


    private void addImage(){
        imageTable = new Table();
        imageTable.setFillParent(true);
        imageTable.add(new Image(Assets.mexZombie)).size(250);
        imageTable.bottom().right();

        stage.addActor(imageTable);
    }

    public void update(float delta) {
        Gdx.gl.glClearColor(ZombrerosGame.BACKGROUND_COLOR.r,
                ZombrerosGame.BACKGROUND_COLOR.g,
                ZombrerosGame.BACKGROUND_COLOR.b,
                1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
        stage.draw();
    }

    private void addTitle() {
        mainTitle = new Label("Zombreros", zombreros.getSkin(), "title");
        mainTitle.setFontScale(2);

        titleTable = new Table();
        titleTable.setFillParent(true);
        titleTable.top();
        titleTable.add(mainTitle).expandX().padTop(50);

        stage.addActor(titleTable);
    }


    private void addButtons() {
        buttonTable = new Table();
        buttonTable.bottom();
        buttonTable.setFillParent(true);
        buttonTable.padBottom(20);

        buttonPlay = new TextButton("\n Play \n", zombreros.getSkin(), "default");
        buttonTable.add(buttonPlay).width(250).padBottom(10);
        buttonTable.row();

        buttonHighscore = new TextButton("\n Highscore \n", zombreros.getSkin(), "default");
        buttonTable.add(buttonHighscore).width(250).padBottom(10);
        buttonTable.row();

        buttonSettings = new TextButton("\n Settings \n", zombreros.getSkin(), "default");
        buttonTable.add(buttonSettings).width(250).padBottom(10);
        buttonTable.row();

        buttonInstructions = new TextButton("\n Instructions \n", zombreros.getSkin(), "default");
        buttonTable.add(buttonInstructions).center().width(250).padBottom(10);
        buttonTable.row();


        stage.addActor(buttonTable);
    }


    private void addListeners() {
        buttonPlay.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.startGame();
                return true;
            }
        });
        buttonHighscore.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onHighscores();
                return true;
            }
        });

        buttonSettings.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onSettings();
                return true;
            }
        });
        buttonInstructions.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                presenter.onInstructions();
                return true;
            }
        });
    }


}
