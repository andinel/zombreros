package com.zombreros.game.model;


import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.model.component.CircleBoundsComponent;
import com.zombreros.game.model.component.HPComponent;
import com.zombreros.game.model.component.PlayerComponent;
import com.zombreros.game.model.component.PositionComponent;
import com.zombreros.game.model.component.RotationComponent;
import com.zombreros.game.model.component.TextureComponent;
import com.zombreros.game.model.component.WallComponent;
import com.zombreros.game.model.component.ZombieComponent;
import com.zombreros.game.model.system.BoundsSystem;
import com.zombreros.game.model.system.BulletFireingSystem;
import com.zombreros.game.model.system.BulletOutOfBoundSystem;
import com.zombreros.game.model.system.MovementSystem;
import com.zombreros.game.model.system.ScoreSystem;
import com.zombreros.game.model.system.TurnSystem;
import com.zombreros.game.model.system.ZombieCollisionSystem;
import com.zombreros.game.model.system.ZombieDamageSystem;
import com.zombreros.game.utils.Assets;
import com.zombreros.game.utils.SoundInterface;

/**
 * Handles game logic
 */
public class GameWorld{
    private PooledEngine engine;


    private MovementSystem movsys;
    private TurnSystem turnsys;

    private GameState gameState;

    private int currentWaveNumber;

    private Wave currentWave;

    private SoundInterface SIC;

    public GameWorld(ZombrerosGame zombreros, SoundInterface SIC) {
        this.engine = new PooledEngine();
        this.SIC = SIC;


        initEntities();
        initSystems();
        Upgradeables.initUpgrades();

        gameState = GameState.WAVE_STATE;

        currentWaveNumber = 1;
        currentWave = new Wave(currentWaveNumber, engine);

    }



    private void initEntities(){

        createBG();

        createWall();

        createPlayer(0,ZombrerosGame.WIDTH/2 - 70,ZombrerosGame.HEIGHT/2-20);
        createPlayer(1,ZombrerosGame.WIDTH/2 + 10,ZombrerosGame.HEIGHT/2-10);


    }

    private void createPlayer(int id, int x, int y){
        Entity player = engine.createEntity();

        PositionComponent position = engine.createComponent(PositionComponent.class);
        RotationComponent rotation = engine.createComponent(RotationComponent.class);
        TextureComponent texture = engine.createComponent(TextureComponent.class);
        PlayerComponent playerID = engine.createComponent(PlayerComponent.class);

        position.pos.x = x;
        position.pos.y = y;
        texture.texture = (id == 0) ? Assets.playerR : Assets.playerG;
        playerID.id = id;

        player.add(position);
        player.add(rotation);
        player.add(playerID);
        player.add(texture);

        engine.addEntity(player);
    }

    public void createBG() {
        Entity bg = engine.createEntity();

        PositionComponent position = engine.createComponent(PositionComponent.class);
        RotationComponent rotation = engine.createComponent(RotationComponent.class);
        TextureComponent texture = engine.createComponent(TextureComponent.class);

        texture.texture = Assets.bg;

        bg.add(position);
        bg.add(rotation);
        bg.add(texture);

        engine.addEntity(bg);

    }

    public void createWall() {

        Entity wall = engine.createEntity();

        WallComponent wallComp = engine.createComponent(WallComponent.class);
        PositionComponent position = engine.createComponent(PositionComponent.class);
        RotationComponent rotation = engine.createComponent(RotationComponent.class);
        TextureComponent texture = engine.createComponent(TextureComponent.class);
        HPComponent hp = engine.createComponent(HPComponent.class);
        CircleBoundsComponent circle = engine.createComponent(CircleBoundsComponent.class);


        texture.texture = Assets.wall;
        position.pos.x = (float)(ZombrerosGame.WIDTH/2 - texture.texture.getRegionWidth()/2);
        position.pos.y = (float) ZombrerosGame.HEIGHT/2 -  texture.texture.getRegionHeight()/2;

        hp.hpMax = Upgradeables.WALL_MAX_HP;
        hp.hp = hp.hpMax;
        hp.hpRed = Assets.hpRed;
        hp.hpGreen = Assets.hpGreen;

        Vector2 wallCenter = BoundsSystem.getTextureCenter(position.pos, texture.texture);
        circle.circle = new Circle(wallCenter, (float) (texture.texture.getRegionWidth()/2-20));

        wall.add(wallComp);
        wall.add(position);
        wall.add(rotation);
        wall.add(texture);
        wall.add(hp);
        wall.add(circle);

        engine.addEntity(wall);
    }

    private void initSystems() {
        Gdx.app.log("initSystems", "");
        movsys = new MovementSystem();
        turnsys = new TurnSystem();
        engine.addSystem(movsys);
        engine.addSystem(turnsys);
        engine.addSystem(new BoundsSystem());
        engine.addSystem(new BulletFireingSystem( engine, SIC));
        engine.addSystem(new ZombieCollisionSystem());
        engine.addSystem(new ZombieDamageSystem(2));
        engine.addSystem(new BulletOutOfBoundSystem(5));
        engine.addSystem(new ScoreSystem(SIC));

    }

    public void updateGame(float dt){
        updateGameState();
        if (!currentWave.isEmpty()) {
            currentWave.update(dt);
        }
        engine.update(dt);
    }

    public ImmutableArray<Entity> getRenderQueue() {
        Family renderable = Family.all(PositionComponent.class, TextureComponent.class, RotationComponent.class).get();
        return engine.getEntitiesFor(renderable);
    }


    public TurnSystem getTurnsys() {
        return turnsys;
    }


    public int getScore(){
        return engine.getSystem(ScoreSystem.class).getScore();
    }
    private void updateGameState() {

        if (isGameOver()) {
           gameState = GameState.GAME_OVER;
        }
        else if (isWaveOver()) {
            Upgradeables.WALLET += getScore();
            Upgradeables.WALL_HP = (int) getWallHPComponent().hp;
            gameState = GameState.UPGRADE_STATE;
            engine.clearPools();
        }
    }

    private boolean isWaveOver() {
        ImmutableArray<Entity> zombies = engine.getEntitiesFor(Family.all(ZombieComponent.class).get());
        return currentWave.isEmpty() && zombies.size() == 0;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }
    private boolean isGameOver() {

        return getWallHPComponent().hp <= 0;
    }

     private HPComponent getWallHPComponent(){
        Entity wall = engine.getEntitiesFor(Family.all(HPComponent.class, WallComponent.class).get()).first();
        return ComponentMapper.getFor(HPComponent.class).get(wall);
    }

    private void setWallHp() {
        HPComponent wallHP = getWallHPComponent();
        wallHP.hp = Upgradeables.WALL_HP;
        wallHP.hpMax = Upgradeables.WALL_MAX_HP;

    }

    public void setNextWave() {
        setGameState(GameState.WAVE_STATE);
        setWallHp();
        currentWaveNumber ++;
        currentWave = new Wave(currentWaveNumber, engine);
        engine.addSystem(new BulletFireingSystem(engine, SIC));
    }

    public int getWaveNumber() {
        return currentWaveNumber;
    }
}
