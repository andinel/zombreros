package com.zombreros.game.model.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.zombreros.game.model.component.PlayerComponent;
import com.zombreros.game.model.component.PositionComponent;
import com.zombreros.game.model.component.RotationComponent;
import com.zombreros.game.model.factories.BulletFactory;
import com.zombreros.game.utils.SoundInterface;

public class BulletFireingSystem extends EntitySystem {
    private static final float BULLET_FREQUENCY  = 1;

    SoundInterface SIC;
    private ImmutableArray<Entity> shooters;

    private BulletFactory bf;
    private float timeTilNextBullet;


    public BulletFireingSystem( PooledEngine engine, SoundInterface SIC) {
        this.SIC = SIC;
        shooters = engine.getEntitiesFor(Family.all(PositionComponent.class, PlayerComponent.class, RotationComponent.class).get());
        bf = new BulletFactory(engine);
        timeTilNextBullet = 5.0f;
    }

    /*
    public void addedToEngine(PooledEngine engine) {
        shooters = engine.getEntitiesFor(Family.all(PositionComponent.class, PlayerComponent.class, RotationComponent.class).get());
        bf = new BulletFactory(engine);
    }*/




    @Override
    public void update(float dt) {
        if(timeTilNextBullet <= 0 ) {
            for (int i = 0; i < shooters.size(); i++) {
                Entity shooter = shooters.get(i);
                bf.createBullet(shooter);
            }
            SIC.playGunshotSound();
            timeTilNextBullet = 1 / BULLET_FREQUENCY;
        }
        else {
            timeTilNextBullet -= dt;
        }

    }
}
