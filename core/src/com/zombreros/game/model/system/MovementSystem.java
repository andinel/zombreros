package com.zombreros.game.model.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.zombreros.game.model.component.PositionComponent;
import com.zombreros.game.model.component.VelocityComponent;


public class MovementSystem extends IteratingSystem {

    private ComponentMapper<PositionComponent> pm;
    private ComponentMapper<VelocityComponent> vm;


    public MovementSystem() {
        super(Family.all(PositionComponent.class, VelocityComponent.class).get());

        pm = ComponentMapper.getFor(PositionComponent.class);
        vm = ComponentMapper.getFor(VelocityComponent.class);
    }

    @Override
    protected void processEntity(Entity entity, float dt) {

        Vector2 pos = pm.get(entity).pos;
        Vector2 vel = vm.get(entity).vel;

        float x = pos.x;
        float y = pos.y;

        pos.set(x + vel.x * dt, y + vel.y * dt);

    }
}
