package com.zombreros.game.model.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.utils.IntArray;
import com.zombreros.game.model.component.PlayerComponent;
import com.zombreros.game.model.component.RotationComponent;

public class TurnSystem extends EntitySystem {

    private ImmutableArray<Entity> players;
    private IntArray turnValues;
    private ComponentMapper<RotationComponent> rm = ComponentMapper.getFor(RotationComponent.class);
    private ComponentMapper<PlayerComponent> pm = ComponentMapper.getFor(PlayerComponent.class);

    public TurnSystem(){

    }

    public void addedToEngine(Engine engine) {
        players =  engine.getEntitiesFor(Family.all(PlayerComponent.class).get());
        turnValues = new IntArray(new int[]{0, 5});
    }

    public void update(float dt) {
        for (int i = 0; i < players.size(); i++) {

            Entity player = players.get(i);
            RotationComponent rot = rm.get(player);

            rot.angle = turnValues.get(i);
        }
    }

    public void setTurn(int angle, int id) {
        turnValues.set(id, turnValues.get(id) + angle);
    }
}
