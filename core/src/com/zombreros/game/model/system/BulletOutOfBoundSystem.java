package com.zombreros.game.model.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalIteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.model.component.BulletComponent;
import com.zombreros.game.model.component.PositionComponent;

/**
 * Remove out of bullets offscreen from engine
 */
public class BulletOutOfBoundSystem extends IntervalIteratingSystem {

    ComponentMapper<PositionComponent> pm;

    public BulletOutOfBoundSystem(float interval) {
        super(Family.all(BulletComponent.class, PositionComponent.class).get(), interval);

        pm = ComponentMapper.getFor(PositionComponent.class);
    }

    @Override
    protected void processEntity(Entity entity) {
        Vector2 pos = new Vector2(pm.get(entity).pos);
        if (isOutOfBounds(pos.x, pos.y)){
            getEngine().removeEntity(entity);
        }

    }

    private boolean isOutOfBounds(float x, float y) {
        return  (-50 > x || x > ZombrerosGame.WIDTH + 50) || (-50 > y || y > ZombrerosGame.HEIGHT + 200) ;
    }


}
