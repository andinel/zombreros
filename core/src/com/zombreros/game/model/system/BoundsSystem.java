package com.zombreros.game.model.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.zombreros.game.model.component.CircleBoundsComponent;
import com.zombreros.game.model.component.PositionComponent;
import com.zombreros.game.model.component.TextureComponent;
import com.zombreros.game.model.component.VelocityComponent;


public class BoundsSystem extends EntitySystem {
    private ComponentMapper<PositionComponent> pm;
    private ComponentMapper<CircleBoundsComponent> bm;
    private ComponentMapper<TextureComponent> tm;

    ImmutableArray<Entity> movingColidables;


    public BoundsSystem() {

        pm = ComponentMapper.getFor(PositionComponent.class);
        bm = ComponentMapper.getFor(CircleBoundsComponent.class);
        tm = ComponentMapper.getFor(TextureComponent.class);
        Gdx.app.log("bounds init", "");

    }

    @Override
    public void addedToEngine(Engine engine) {
        movingColidables = engine.getEntitiesFor(Family.all(CircleBoundsComponent.class,
                PositionComponent.class,
                TextureComponent.class,
                VelocityComponent.class).get());

    }

    @Override
    public void update(float deltaTime) {

        for (int i = 0; i < movingColidables.size(); i++) {
            Entity col = movingColidables.get(i);
            PositionComponent pos =  pm.get(col);
            CircleBoundsComponent bounds =  bm.get(col);
            TextureComponent texture = tm.get(col);
            Vector2 textureCenter = getTextureCenter(pos.pos, texture.texture );
            
            
            bounds.circle.x = textureCenter.x; // - bounds.circle.width * 0.5f;
            bounds.circle.y = textureCenter.y; // - bounds.bounds.height * 0.5f;



        }
    }

    public static Vector2 getTextureCenter(Vector2 texPos, TextureRegion texture) {

        Vector2 texcenter =  new Vector2(texture.getRegionWidth()/2.0f, texture.getRegionHeight()/2.0f);
        return texPos.cpy().add(texcenter);
    }
}

