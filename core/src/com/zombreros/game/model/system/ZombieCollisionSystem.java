package com.zombreros.game.model.system;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.zombreros.game.model.component.BulletComponent;
import com.zombreros.game.model.component.CircleBoundsComponent;
import com.zombreros.game.model.component.DamageComponent;
import com.zombreros.game.model.component.HPComponent;
import com.zombreros.game.model.component.VelocityComponent;
import com.zombreros.game.model.component.WallComponent;
import com.zombreros.game.model.component.ZombieComponent;

public class ZombieCollisionSystem extends EntitySystem {

    private ComponentMapper<DamageComponent> dm;
    private ComponentMapper<VelocityComponent> vm;
    private ComponentMapper<CircleBoundsComponent> cm;
    ComponentMapper<HPComponent> hm;

    private ImmutableArray<Entity> bullets;
    private ImmutableArray<Entity> zombies;
    private ImmutableArray<Entity> walls;

    Engine engine;


    public ZombieCollisionSystem() {

        vm = ComponentMapper.getFor(VelocityComponent.class);
        cm = ComponentMapper.getFor(CircleBoundsComponent.class);
        dm = ComponentMapper.getFor(DamageComponent.class);
        hm = ComponentMapper.getFor(HPComponent.class);
    }


    @Override
    public void addedToEngine(Engine engine){
        this.engine = engine;

        bullets = engine.getEntitiesFor(Family.all(BulletComponent.class, DamageComponent.class , CircleBoundsComponent.class).get());
        zombies = engine.getEntitiesFor(Family.all(ZombieComponent.class, HPComponent.class, CircleBoundsComponent.class).get());
        walls = engine.getEntitiesFor(Family.all(WallComponent.class).get());
    }



    @Override
    public void update( float deltaTime) {
        //double loop: O(n^2), any faster way?
        for (int i = 0; i < zombies.size(); i++) {
            Entity zombie = zombies.get(i);


            for (int j = 0; j <  bullets.size(); j++) {

                Entity bullet = bullets.get(j);
                // check collision
                if(cm.get(bullet).circle.overlaps(cm.get(zombie).circle)) {
                    Gdx.app.log("hit", "hit");
                    hm.get(zombie).hp -= dm.get(bullet).damage;
                    engine.removeEntity(bullet);

                }


            }

            Entity wall = walls.first();
            if (cm.get(wall).circle.overlaps(cm.get(zombie).circle)) {
                // zombie stops when attacking wall
                zombie.remove(VelocityComponent.class);
            }
        }
    }


}
