package com.zombreros.game.model.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.zombreros.game.model.component.HPComponent;
import com.zombreros.game.model.component.ZombieComponent;
import com.zombreros.game.utils.SoundInterface;

public class ScoreSystem extends IteratingSystem {
    ComponentMapper<HPComponent> hm;
    ComponentMapper<ZombieComponent> zm;
    private int score;
    SoundInterface SIC;

    public ScoreSystem(SoundInterface SIC) {
        super(Family.all(ZombieComponent.class, HPComponent.class).get());

        hm = ComponentMapper.getFor(HPComponent.class);
        zm = ComponentMapper.getFor(ZombieComponent.class);
        this.score = 0;
        this.SIC = SIC;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        if( hm.get(entity).hp <= 0 ) {
            SIC.playZombieDeathSound();
            getEngine().removeEntity(entity);
            score += zm.get(entity).value;
            System.out.println(score);

            }
        }

    public int getScore() {
        return score;
    }
}



