package com.zombreros.game.model.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.zombreros.game.model.component.CircleBoundsComponent;
import com.zombreros.game.model.component.DamageComponent;
import com.zombreros.game.model.component.HPComponent;
import com.zombreros.game.model.component.VelocityComponent;
import com.zombreros.game.model.component.ZombieComponent;

public class ZombieDamageSystem extends IntervalSystem {

    private ComponentMapper<DamageComponent> dm;
    private ComponentMapper<HPComponent> hm;

   ImmutableArray<Entity> staticZombies;
   ImmutableArray<Entity> walls;

    public ZombieDamageSystem(float interval) {
        super(interval);
        dm = ComponentMapper.getFor(DamageComponent.class);
        hm = ComponentMapper.getFor(HPComponent.class);

    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        staticZombies = engine.getEntitiesFor(Family.all(ZombieComponent.class, DamageComponent.class)
                .exclude(VelocityComponent.class).get());
        walls = engine.getEntitiesFor(Family.all(HPComponent.class, CircleBoundsComponent.class)
                .exclude(VelocityComponent.class).get());


    }

    @Override
    protected void updateInterval() {


        Entity wall = walls.first();

        for (int i = 0; i < staticZombies.size(); i++) {
            hm.get(wall).hp -=  dm.get(staticZombies.get(i)).damage;

        }

    }
}
