package com.zombreros.game.model;

public enum GameState {
    WAVE_STATE, UPGRADE_STATE, GAME_OVER, PAUSE
}
