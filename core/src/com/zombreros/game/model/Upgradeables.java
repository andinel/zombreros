package com.zombreros.game.model;

public class Upgradeables {

    public static int BULLET_DAMAGE; //done
    public static int BULLET_DAMAGE_LEVEL;
    public static int BULLET_SPEED; // done
    public static int BULLET_SPEED_LEVEL = 1;
    public static int WALL_HP = 150; //done
    public static int WALL_LEVEL = 1;
    public static int WALL_MAX_HP = 200;
    public static int WALLET = 500;

    public static int REPAIR_PERCENTAGE = 20;
    public static int REPAIR_PRICE = 100;
    public static int PRICE_PER_LEVEL = 75;
    public static int WALL_UPGRADE = 20;
    public static int BULLET_DAMAGE_UPGRADE = 5;
    public static int BULLET_SPEED_UPGRADE = 20;

    // set upgradabels to start vaules for a game
    public static void initUpgrades() {
        BULLET_DAMAGE = 5;
        BULLET_SPEED = 200;
        WALL_MAX_HP = 200;
        WALL_HP = WALL_MAX_HP;
        WALLET = 500;

        BULLET_SPEED_LEVEL = 1;
        BULLET_DAMAGE_LEVEL = 1;
        WALL_LEVEL = 1;

    }
}
