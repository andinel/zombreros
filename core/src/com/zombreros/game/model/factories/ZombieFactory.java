package com.zombreros.game.model.factories;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.model.component.CircleBoundsComponent;
import com.zombreros.game.model.component.DamageComponent;
import com.zombreros.game.model.component.HPComponent;
import com.zombreros.game.model.component.PositionComponent;
import com.zombreros.game.model.component.RotationComponent;
import com.zombreros.game.model.component.TextureComponent;
import com.zombreros.game.model.component.VelocityComponent;
import com.zombreros.game.model.component.ZombieComponent;
import com.zombreros.game.utils.Assets;

import java.util.Random;

/**
 * Used by
 */
public abstract class  ZombieFactory {

    Random rand;
    private PooledEngine engine;

    public ZombieFactory(PooledEngine engine){

        this.engine = engine;
        rand = new Random();
    }

    public abstract Entity createZombie();


    protected   Entity build(float zombieHp, int zombieSpeed , TextureRegion zombieTextureR, char zombieType, float zombieDmg){

        Entity zombie = engine.createEntity();

        PositionComponent position = engine.createComponent(PositionComponent.class);
        RotationComponent rotation = engine.createComponent(RotationComponent.class);
        VelocityComponent velocity = engine.createComponent(VelocityComponent.class);
        TextureComponent texture = engine.createComponent(TextureComponent.class);
        ZombieComponent zomb = engine.createComponent(ZombieComponent.class);
        CircleBoundsComponent circle = engine.createComponent(CircleBoundsComponent.class);
        HPComponent hp =  engine.createComponent(HPComponent.class);
        DamageComponent dmg = engine.createComponent(DamageComponent.class);

        position.pos.x = getRandomXPos();
        position.pos.y = getRandomYPos();

        Vector2 vel = getVelocity(position.pos, zombieSpeed);
        rotation.angle = vel.angleDeg();

        velocity.vel.set(vel);

        texture.texture = zombieTextureR;
        circle.circle = new Circle(position.pos, 20);

        hp.hpMax = zombieHp;
        hp.hp = hp.hpMax;
        hp.hpRed = Assets.hpRed;
        hp.hpGreen = Assets.hpGreen;

        dmg.damage = zombieDmg;
        zomb.type = zombieType;


        zombie.add(position);
        zombie.add(rotation);
        zombie.add(velocity);
        zombie.add(texture);
        zombie.add(zomb);
        zombie.add(hp);
        zombie.add(circle);
        zombie.add(dmg);
        
        return zombie;
    }




    protected Vector2 getVelocity(Vector2 pos, int zombieSpeed) {
        float centerX =  ZombrerosGame.WIDTH/2.0f;
        float centerY= ZombrerosGame.HEIGHT/2.0f;
        Vector2 vec = new Vector2(centerX - pos.x, centerY - pos.y);

        return vec.setLength(zombieSpeed);
    }


    protected int getRandomYPos(){
        return rand.nextInt(ZombrerosGame.HEIGHT + 1);
    }

    /**
     * Determines if zombie is spawned on the left or right side of the screen (outside)
     * @return x position of the spawn
     */
    protected int getRandomXPos(){

        return rand.nextBoolean() ? -50 : ZombrerosGame.WIDTH + 50;

    }
}
