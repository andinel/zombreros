package com.zombreros.game.model.factories;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.zombreros.game.utils.Assets;

public class BasicZombieFactory extends ZombieFactory {

    public final int BASIC_ZOMBIE_SPEED =  20;
    public final float BASIC_ZOMBIE_HP = 15;
    public final float BASIC_ZOMBIE_DMG = 10;
    public final char BASIC_ZOMBIE_TYPE = 'b';
    private final TextureRegion basicZombTexture = Assets.getBasicZombie();


    public BasicZombieFactory(PooledEngine engine) {
        super(engine);
    }

    @Override
    public Entity createZombie() {
        return build(BASIC_ZOMBIE_HP, BASIC_ZOMBIE_SPEED, basicZombTexture, BASIC_ZOMBIE_TYPE , BASIC_ZOMBIE_DMG);
    }
}
