package com.zombreros.game.model.factories;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.zombreros.game.utils.Assets;

public class HeavyZombieFactory extends ZombieFactory {

    public final int HEAVY_ZOMBIE_SPEED =  10;
    public final float HEAVY_ZOMBIE_HP = 50;
    public final float HEAVY_ZOMBIE_DMG = 40;
    public final char HEAVY_ZOMBIE_TYPE = 'h';
    private final TextureRegion heavyZombTexture = Assets.bigZombie;


    public HeavyZombieFactory(PooledEngine engine) {
        super(engine);
    }

    @Override
    public Entity createZombie() {
        return build(HEAVY_ZOMBIE_HP, HEAVY_ZOMBIE_SPEED, heavyZombTexture, HEAVY_ZOMBIE_TYPE , HEAVY_ZOMBIE_DMG);
    }
}
