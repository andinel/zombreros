package com.zombreros.game.model.factories;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.zombreros.game.model.Upgradeables;
import com.zombreros.game.model.component.BulletComponent;
import com.zombreros.game.model.component.CircleBoundsComponent;
import com.zombreros.game.model.component.DamageComponent;
import com.zombreros.game.model.component.PositionComponent;
import com.zombreros.game.model.component.RotationComponent;
import com.zombreros.game.model.component.TextureComponent;
import com.zombreros.game.model.component.VelocityComponent;
import com.zombreros.game.utils.Assets;


public class BulletFactory {

    private ComponentMapper<PositionComponent> pm = ComponentMapper.getFor(PositionComponent.class);
    private ComponentMapper<RotationComponent> rm = ComponentMapper.getFor(RotationComponent.class);
    private ComponentMapper<TextureComponent> tm = ComponentMapper.getFor(TextureComponent.class);

    private PooledEngine engine;

    public BulletFactory(PooledEngine engine) {
        this.engine = engine;

    }

    public void createBullet(Entity shooter) {

        float BULLET_RADIUS = 3.0f;

        PositionComponent shooterPos = pm.get(shooter);
        RotationComponent shooterRot = rm.get(shooter);
        TextureComponent shooterTex = tm.get(shooter);

        Vector2 vel = getVelocity(shooterRot.angle);

        Entity bullet = engine.createEntity();

        RotationComponent rotation = engine.createComponent(RotationComponent.class);
        PositionComponent position = engine.createComponent(PositionComponent.class);
        VelocityComponent velocity = engine.createComponent(VelocityComponent.class);
        DamageComponent damage = engine.createComponent(DamageComponent.class);
        TextureComponent texture = engine.createComponent(TextureComponent.class);
        CircleBoundsComponent circle = engine.createComponent(CircleBoundsComponent.class);
        BulletComponent bul = engine.createComponent(BulletComponent.class);

        velocity.vel.set(vel);

        // Make bullet exit gun
        Vector2 gunpos = shooterPos.pos.cpy().add(shooterTex.texture.getRegionWidth()/2.0f,
                shooterTex.texture.getRegionHeight()/2.0f);
        Vector2 gunOffset = new Vector2(24, -10);
        gunOffset.setAngleDeg(shooterRot.angle-20);
        position.pos.set(gunpos.add(gunOffset));

        damage.damage = Upgradeables.BULLET_DAMAGE;
        texture.texture = Assets.bullet;
        circle.circle = new Circle(new Vector2(position.pos), BULLET_RADIUS);


        bullet.add(rotation);
        bullet.add(position);
        bullet.add(velocity);
        bullet.add(damage);
        bullet.add(texture);
        bullet.add(bul);
        bullet.add(circle);

        engine.addEntity(bullet);

    }

    /**
     * Returns the speed components of the bullet based on the angle of the shooter (vx, vy)
     * @param angle - angle of shooter of the x plane
     * @return Vector2 - velocity vector of the bullet
     */
    public Vector2 getVelocity(float angle) {
        Vector2 vec = new Vector2(Upgradeables.BULLET_SPEED, 0);
        vec.setAngleDeg(angle);
        return vec;

    }
}
