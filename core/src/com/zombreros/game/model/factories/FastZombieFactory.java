package com.zombreros.game.model.factories;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.zombreros.game.utils.Assets;

public class FastZombieFactory extends ZombieFactory {

    public final int FAST_ZOMBIE_SPEED =  40;
    public final float FAST_ZOMBIE_HP = 10;
    public final float FAST_ZOMBIE_DMG = 5;
    public final char FAST_ZOMBIE_TYPE = 'b';
    private final TextureRegion fastZombTexture = Assets.getFastZombie();


    public FastZombieFactory(PooledEngine engine) {
        super(engine);
    }

    @Override
    public Entity createZombie() {
        return build(FAST_ZOMBIE_HP, FAST_ZOMBIE_SPEED, fastZombTexture, FAST_ZOMBIE_TYPE , FAST_ZOMBIE_DMG);
    }
}
