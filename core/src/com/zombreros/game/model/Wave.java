package com.zombreros.game.model;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.zombreros.game.model.factories.BasicZombieFactory;
import com.zombreros.game.model.factories.FastZombieFactory;
import com.zombreros.game.model.factories.HeavyZombieFactory;
import com.zombreros.game.model.factories.ZombieFactory;

import java.util.Random;
import java.util.Stack;

public class Wave {



    private Stack<Entity> enemies;

    private float nextSpawnTime;

    private float timeSinceLastSpawn;

    private int waveNumber;

    private Random rand;

    private PooledEngine engine;

    private final float minSpawnTime;
    private final float maxSpawnTime;


    public Wave(int waveNumber, PooledEngine engine) {

        this.waveNumber = waveNumber;
        this.engine = engine;

        //first enemy spawns after 3 sec
        this.nextSpawnTime = 3.00f;

        timeSinceLastSpawn = 0;

        minSpawnTime = Math.max(5.0f - waveNumber*0.1f, 1);
        maxSpawnTime = Math.max(10.0f - waveNumber*0.1f, 3);

        rand = new Random();

        enemies = generateEnemies();


    }


    private Stack<Entity> generateEnemies() {

        Stack<Entity> enemies = new Stack<>();
        ZombieFactory bzf = new BasicZombieFactory(engine);
        ZombieFactory fzf;
        ZombieFactory hzf;

        int enemyNumber = 4 + 2*waveNumber;
        int i = 0;


        while (i < enemyNumber){

            if (waveNumber > 6) {
                hzf = new HeavyZombieFactory(engine);

                if (rand.nextFloat() < 0.2) {
                    enemies.push(hzf.createZombie());
                    i++;
                    continue;
                }
            }
            if (waveNumber > 3) {
                fzf = new FastZombieFactory(engine);
                if (rand.nextFloat() < 0.3) {
                    enemies.push(fzf.createZombie());
                    i++;
                    continue;
             }



            }
            enemies.push(bzf.createZombie());
            i++;

        }


        return enemies;
    }


    public void update(float dt) {
        if (timeSinceLastSpawn < nextSpawnTime){
            timeSinceLastSpawn += dt;
        }
        else {

            engine.addEntity(enemies.pop());
            timeSinceLastSpawn = 0;
            // draw next spawn time in range
            nextSpawnTime = getRandNextSpawnTime(minSpawnTime, maxSpawnTime);

        }

    }


    /**
     * checks if there is an more enemies queued
     * @return returns true if enemy stack is empty, false otherwise
     */
    public boolean isEmpty(){
        return enemies.isEmpty();
    }


    private float getRandNextSpawnTime(float minTime, float maxTime){
        return rand.nextFloat()* (maxTime-  minTime) + minTime;
    }

}
