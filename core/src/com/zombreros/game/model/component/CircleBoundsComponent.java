package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.utils.Pool.Poolable;

public class CircleBoundsComponent implements Component, Poolable {

    public Circle circle = new Circle();

    @Override
    public void reset() {
        circle.radius = 0.0f;
        circle.x = 0.0f;
        circle.y = 0.0f;
    }
}
