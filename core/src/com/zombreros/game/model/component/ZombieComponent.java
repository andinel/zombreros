package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool.Poolable;

public class ZombieComponent implements Component, Poolable {
    public char type = '\0';
    public int  value = 20;
    @Override
    public void reset() {
        type = '\0';
        value = 20;
    }
}
