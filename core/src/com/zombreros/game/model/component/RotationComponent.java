package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool.Poolable;

public class RotationComponent implements Component, Poolable {

    public float angle = 0.0f;

    @Override
    public void reset() {
        angle = 0.0f;
    }
}
