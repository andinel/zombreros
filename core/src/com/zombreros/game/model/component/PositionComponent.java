package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;

public class PositionComponent implements Component, Poolable {

    public Vector2 pos = new Vector2();

    @Override
    public void reset() {
        pos.x = 0.0f;
        pos.y = 0.0f;
    }
}
