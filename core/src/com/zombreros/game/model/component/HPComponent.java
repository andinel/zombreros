package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool.Poolable;

public class HPComponent implements Component, Poolable {

    public float hpMax = 0.0f;
    public float hp = 0.0f;

    public TextureRegion hpRed = null;
    public TextureRegion hpGreen = null;

    @Override
    public void reset() {
        hpMax = 0.0f;
        hp = 0.0f;

        hpRed = null;
        hpGreen = null;
    }
}
