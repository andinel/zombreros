package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool.Poolable;

public class PlayerComponent implements Component, Poolable {

    public int id = 0;

    @Override
    public void reset() {
        id = 0;
    }
}
