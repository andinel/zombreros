package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;

public class VelocityComponent implements Component, Poolable {

    public Vector2 vel = new Vector2();

    @Override
    public void reset() {
        vel.x = 0.0f;
        vel.y = 0.0f;
    }
}
