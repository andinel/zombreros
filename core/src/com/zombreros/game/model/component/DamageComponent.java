package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool.Poolable;

public class DamageComponent implements Component, Poolable {

    public float damage = 10.0f;

    @Override
    public void reset() {
        damage = 0.0f;
    }
}
