package com.zombreros.game.model.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool.Poolable;

public class TextureComponent implements Component, Poolable {

    public TextureRegion texture = null;

    public TextureRegion getTexture(){
        return texture;
    }

    @Override
    public void reset() {
        texture = null;
    }
}

