package com.zombreros.game.desktop;

import com.zombreros.game.utils.HighScores;
import com.zombreros.game.utils.FireBaseInterface;

public class DesktopInterfaceClass implements FireBaseInterface {

    @Override
    public void registerNewScore(String nickname, int score){}

    @Override
    public void SetOnValueChangedListener(HighScores highscores){}
}
