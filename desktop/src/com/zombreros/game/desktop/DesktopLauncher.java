package com.zombreros.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.zombreros.game.ZombrerosGame;
import com.zombreros.game.utils.SoundInterfaceClass;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = ZombrerosGame.WIDTH;
		config.height = ZombrerosGame.HEIGHT;
		config.title = ZombrerosGame.TITLE;

		new LwjglApplication(new ZombrerosGame(new DesktopInterfaceClass(), new SoundInterfaceClass()), config);

	}
}
