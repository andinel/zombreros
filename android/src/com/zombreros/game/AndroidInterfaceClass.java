package com.zombreros.game;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.zombreros.game.utils.FireBaseInterface;
import com.zombreros.game.utils.HighScores;
import com.zombreros.game.utils.Score;

import static android.content.ContentValues.TAG;

public class AndroidInterfaceClass implements FireBaseInterface {
    DatabaseReference mDatabase;
    DatabaseReference scoreRef;

    public AndroidInterfaceClass() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        scoreRef = FirebaseDatabase.getInstance().getReference("scores");
    }

    @Override
    public void registerNewScore(String nickname, int score) {
        Score newScore = new Score(nickname, score);
        //Registers a new score and nickname with an unique id
        mDatabase.child("scores").push().setValue(newScore);
    }

    @Override
    public void SetOnValueChangedListener(final HighScores highscores) {
        scoreRef.orderByChild("score").limitToLast(10).addValueEventListener(new ValueEventListener() {
            // Read from the database

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()){
                    String nickname = "";
                    int score = 0;
                    int counter = 1;
                    // Ignoring the unique IDs and only getting the nicknames and scores for each child
                    for (DataSnapshot childchildSnapshot: childSnapshot.getChildren()) {
                        if(counter == 1){
                            nickname = childchildSnapshot.getValue(String.class);
                        }
                        else{
                            score = childchildSnapshot.getValue(Long.class).intValue();
                        }
                        counter++;
                    }
                    highscores.addScore(nickname, score);
                }
                //Making sure the array is sorted in a descending order (highest score first)
                highscores.sortArray();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

}